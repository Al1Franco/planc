import { Schema, model } from 'mongoose';

export const SubjectSchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  description:  {
    type: String,
    required: true
  }
});

export const Subject = model('Subject', SubjectSchema);
