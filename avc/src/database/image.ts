import { Schema, model } from 'mongoose';
import { PointSchema, FileSchema } from './common';

export const ImageSchema = new Schema({
  code: {type:String, required: true},
  title: {type:String, required: false},
  description: {type:String, required: false},
  file: {
    type: FileSchema,
    target: 'images',
    required: true,
    uiMeta: {
      uiField: 'imagefile',
      locationTarget: 'latlon',
      // linkedWith: 'resource in plural'
    }
  },
  galleries: {
    type: [String],
    uiMeta: {
      uiField: 'multiselect',
      linkedWith: 'galleries',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
  author: String,
  latlon: {
    type: PointSchema,
    uiMeta: {
      uiField: 'latlong'
    }
  },
  company: String,
  studio: String,
  source_file: String,
  copyright: String,
  creation_date: Date,
  date_notes: String,
  avc_start_date: String,
  //////
  subjects: {
    type: [String],
    uiMeta: {
      uiField: 'multiselect',
      linkedWith: 'subjects',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
  colection: String,
  editorial: String,
  related_texts: String,
  document_type: String,
  format: String,
  biblio_references: String,
  avc_notes: String,
  keywords: [String],
  categories: [String],
  related_projects: [String],
});

export const Image = model('Image', ImageSchema);




