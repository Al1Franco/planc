import { Schema, model } from 'mongoose';

export const SiteTypeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    uiField: 'content'
  }
});

export const SiteType = model('SiteType', SiteTypeSchema);
