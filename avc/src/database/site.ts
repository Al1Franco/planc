import { Schema, model } from 'mongoose';
import { PointSchema, FileSchema } from './common';

export const SiteSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    uiField: 'content'
  },
  type: {
    type: String,
    required: true,
    uiMeta: {
      uiField: 'select',
      linkedWith: 'sitetypes',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
  polygon: {
    type: String,
    required: false,
    uiMeta: {
      uiField: 'select',
      linkedWith: 'polygons',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
  file: {
    type: FileSchema,
    target: 'map/images',
    required: true,
    uiMeta: {
      uiField: 'imagefile',
      locationTarget: 'latlon'
    }
  },
  latlon: {
    type: PointSchema,
    uiMeta: {
      uiField: 'latlong'
    }
  },
  path: {
    type: String,
    uiMeta: {
      uiField: 'select',
      linkedWith: 'routes',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
});

export const Site = model('Site', SiteSchema);
