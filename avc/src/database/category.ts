import { Schema, model } from 'mongoose';

export const CategorySchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  description:  String
});

export const Category = model('Category', CategorySchema);
