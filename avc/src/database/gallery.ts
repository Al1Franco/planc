import { Schema, model } from 'mongoose';

export const GallerySchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  description:  String,
  status: {
    type: String,
    enum: [
      'private',
      'public'
    ],
    default: 'private'
  }
});

export const Gallery = model('Gallery', GallerySchema);
