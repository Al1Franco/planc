import { Schema, model } from 'mongoose';

export const PolygonSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
    uiField: 'content'
  }
});

export const Polygon = model('Polygon', PolygonSchema);
