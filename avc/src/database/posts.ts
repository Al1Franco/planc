import { Schema, model } from 'mongoose';

export const PostSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  excerpt: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true,
    uiMeta: {
      uiField: 'content',
    }
  },
  slug: {
    type: String,
    required: true
  },
  terms: {
    type: [String]
  },
  post_status: {
    type: String,
    enum: [
      'published',
      'unpublished'
    ]
  },
  created: {
    type: Date
  },
  updated: {
    type: Date
  },
});

export const Post = model('Post', PostSchema);
