import { Schema, model } from 'mongoose';
import { FileSchema } from './common';

export const RouteSchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  description:  {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true,
    uiMeta: {
      uiField: 'select',
      linkedWith: 'sitetypes',
      keyLabel: {key: '_id', label: 'name'}
    }
  },
  photo_gallery:  {
    type: String,
    required: true
  },
  audio: {
    type: FileSchema,
    target: 'map/audios',
    required: false,
    uiMeta: {
      uiField: 'file',
      uiIcon: 'mdi-account-music'
    }
  },
  status: {
    type: String,
    enum: [
      'private',
      'public'
    ],
    default: 'private'
  },
});

export const Route = model('Route', RouteSchema);
