
const DATE_RE = /(?<day>\d{1,2}) de (?<month>\w+) del (?<year>\d{4})/;
const HOUR_RE = /(?<hours>\d{1,2}):(?<minutes>\d{1,2})\s+(?<ampm>[am|pm])/;

const MONTHS = {
  enero: 1,
  febrero: 2,
  marzo: 3,
  abril: 4,
  mayo: 5,
  junio: 6,
  julio: 7,
  agosto: 8,
  septiembre: 9,
  octubre: 9,
  noviembre: 11,
  diciembre: 12
}

function toTime(dateStr, timeStr) {
  const processed = dateStr.match(DATE_RE);
  const processed2 = (timeStr && timeStr.match(HOUR_RE)) || {};
  if (processed.groups) {
    const day = processed.groups.day;
    const month = MONTHS[processed.groups.month];
    const year = processed.groups.year;
    if (processed2.groups) {
      const hours = parseInt(processed2.groups.hours);
      const minutes = parseInt(processed2.groups.minutes);
      const ampm = processed2.groups.ampm;
      return new Date(`${year}-${month}-${day}T${ampm === 'pm' ? hours + 12 : hours}:${minutes}:00`);
    }
    return new Date(`${year}-${month}-${day}`);
  }
  return dateStr;
}

export const state = () => ({
  items: [],
  indexedItems: {},
});

export const mutations = {
  setPosts(state, items) {
    state.items = items;
    state.items.forEach(item => state.indexedItems[item.slug] = item);
  }
};

export const actions = {
  async loadPosts({state, commit}, page) {
    const response = await this.$axios.$get(`/api/v1/posts/paginated?page=${page}&pageCount=${10}&wp_info.thumbnail.link={"$ne":false}`);
    if (response && response.errors && response.errors.length) {
      return []
    }
    const posts = await Promise.all(response.data.values.map(async gallery => {
      const response = await this.$axios.$get(`/api/v1/images?posts=${gallery._id}`);
      if (response && response.errors && response.errors.length) {
        return []
      }
      const galleryImages = response.data.values.map(image => ({
        ...image,
        image: image.file.path.split('?')[0]
      }));
      const preview = {
        title: gallery.name,
        title: gallery.description,
        image: galleryImages.length ? galleryImages[0] : ''
      };
      return {
        ...gallery,
        images: galleryImages,
        preview
      };
    }))
    const pposts = posts.map(item => {
      const result = {
        id: item._id,
        label: item.terms[0],
        title: item.title,
        slug: item.slug,
        content: item.content,
        img: item.wp_info.thumbnail.link,
        more: item
      };
      if (item.terms && (item.terms.includes('Agenda') || item.terms.includes('agenda'))) {
        const data = item.wp_info.content.split('\n').map(val => val.trim());
        const whenDate = data.filter(val => val.match(DATE_RE)).pop();
        const whenTime = data.filter(val => val.match(HOUR_RE)).pop();
        try {
          if(whenDate) {
            result.time = toTime(whenDate, whenTime);
          }
        } catch (error) {
          console.log(error);
        }
      }
      return result;
    }).filter(elm => elm.title !== 'agenda');
    commit('setPosts', pposts);
    return state.items;
  },
  async getPosts({state}, {page, count}) {
    const response = await this.$axios.$get(`/api/v1/posts/paginated?page=${page}&pageCount=${count}&wp_info.thumbnail.link={"$ne":false}`);
    if (response && response.errors && response.errors.length) {
      return []
    }
    const posts = await Promise.all(response.data.values.map(async gallery => {
      const response = await this.$axios.$get(`/api/v1/images?posts=${gallery._id}`);
      if (response && response.errors && response.errors.length) {
        return []
      }
      const galleryImages = response.data.values.map(image => ({
        ...image,
        image: image.file.path.split('?')[0]
      }));
      const preview = {
        title: gallery.name,
        title: gallery.description,
        image: galleryImages.length ? galleryImages[0] : ''
      };
      return {
        ...gallery,
        images: galleryImages,
        preview
      };
    }))
    const pposts = posts.map(item => {
      const result = {
        id: item._id,
        label: item.terms[0],
        title: item.title,
        slug: item.slug,
        content: item.content,
        img: item.wp_info.thumbnail.link,
        more: item
      };
      if (item.terms && (item.terms.includes('Agenda') || item.terms.includes('agenda'))) {
        const data = item.wp_info.content.split('\n').map(val => val.trim());
        const whenDate = data.filter(val => val.match(DATE_RE)).pop();
        const whenTime = data.filter(val => val.match(HOUR_RE)).pop();
        try {
          if(whenDate) {
            result.time = toTime(whenDate, whenTime);
          }
        } catch (error) {
          console.log(error);
        }
      }
      return result;
    }).filter(elm => elm.title !== 'agenda');
    return pposts;
  }
};
