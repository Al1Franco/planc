module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs', 'plugin:nuxt/recommended', "@vue/prettier"
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    quotes: ['error', 'single', { avoidEscape: true }],
    'comma-dangle': ['error', 'always'],
    semi: ['error', 'always'],
    'vue/html-self-closing': 'off'
  },
  plugins: [],
  // add your custom rules here
  rules: {},
};
