
export const state = () => ({
  item: {},
  items: [],
  indexedItems: {},
});

export const mutations = {
  setGalleries(state, items) {
    state.items = items;
    items.forEach(item => {
      state.indexedItems[item._id] = item;
    });
  },
  setGallery(state, item) {
    state.item = item;
    state.indexedItems[item._id] = item;
  }
};

export const actions = {
  async loadGallery({state, commit}, id) {
    if (state.indexedItems[id]) {
      commit('setGallery', state.indexedItems[id]);
      return state.indexedItems[id];
    }
    const response = await this.$axios.$get(`/api/v1/galleries/${id}`);
    if (response && response.errors && response.errors.length) {
      return []
    }
    const gallery = response.data.value;
    {
      const response = await this.$axios.$get(`/api/v1/images?galleries=${gallery._id}`);
      if (response && response.errors && response.errors.length) {
        return []
      }
      const galleryImages = response.data.values.map(image => ({
        ...image,
        image: image.file.path.split('?')[0]
      }));
      const preview = {
        title: gallery.name,
        title: gallery.description,
        image: galleryImages.length ? galleryImages[0] : ''
      };
      gallery.images = galleryImages;
      gallery.preview = preview;
    }
    commit('setGallery', gallery);
    return state.item;
  },
  async loadGalleries({state, commit}) {
    const response = await this.$axios.$get('/api/v1/galleries');
    if (response && response.errors && response.errors.length) {
      return []
    }
    const galleries = await Promise.all(response.data.values.map(async gallery => {
      const response = await this.$axios.$get(`/api/v1/images?galleries=${gallery._id}`);
      if (response && response.errors && response.errors.length) {
        return []
      }
      const galleryImages = response.data.values.map(image => ({
        ...image,
        image: image.file.path.split('?')[0]
      }));
      const preview = {
        title: gallery.name,
        title: gallery.description,
        image: galleryImages.length ? galleryImages[0] : ''
      };
      return {
        ...gallery,
        images: galleryImages,
        preview
      };
    }))
    commit('setGalleries', galleries);
    return state.items;
  }
};


