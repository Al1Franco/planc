
export const state = () => ({
  item: {},
  items: [],
  indexedItems: {},
});

export const mutations = {
  setImages(state, items) {
    state.items = items;
    items.forEach(item => {
      state.indexedItems[item._id] = item;
    });
  },
  setImage(state, item) {
    state.item = item;
    state.indexedItems[item._id] = item;
  }
};

export const actions = {
  async loadImage({commit, state}, id) {
    if (state.indexedItems[id]) {
      commit('setImage', state.indexedItems[id]);
      return state.item;
    }
    const response = await this.$axios.$get(`/api/v1/images/${id}`);
    if (response && response.errors && response.errors.length) {
      return {}
    }
    const image = response.data.value;
    image.image = image.file.path.split('?')[0];
    commit('setImage', image);
    return state.item;
  },
  async loadImages({commit}) {
    const response = await this.$axios.$get('/api/v1/images');
    if (response && response.errors && response.errors.length) {
      return []
    }
    const images = response.data.values.map(image => ({
      ...image,
      image: image.file.path.split('?')[0]
    }));
    commit('setImages', images);
    return state.items;
  }
};
