
export const state = () => ({
  items: [],
  indexedItems: {},
});

export const mutations = {
  setSubjects(state, items) {
    state.items = items;
    state.items.forEach(item => state.indexedItems[item._id] = item);
  }
};

export const actions = {
  async loadSubjects({state, commit}) {
    const response = await this.$axios.$get('/api/v1/subjects');
    if (response && response.errors && response.errors.length) {
      return []
    }
    const subjects = response.data.values;
    commit('setSubjects', subjects);
    return state.items;
  }
};


