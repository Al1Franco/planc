
export const state = () => ({
  menuItems: [
    {
      name: 'Home',
      type: 'link',
      link: '/',
    },
    {
      name: 'Archivo',
      type: 'link',
      link: '/archive',
    },
  ],
});

